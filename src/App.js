import React, { Component } from "react";
import Header from "./Header.js";

class App extends Component {
  state = { data: null }; /*----------------A*/

  componentDidMount() {
    fetch("https://jsonplaceholder.typicode.com/posts?userId=1")
      .then((rst) => rst.json())
      .then((rst) => this.setState({ data: rst })) //----------------B//
      .catch((err) => {
        console.log(err);
      });
  }

  render() {
    const { data } = this.state; //---------------- C
    return (
      <div>
        <Header title="App Page" />
        <h4>Sample FetchData</h4>

        {data &&
          data.map((d) => {
            //---------------- D
            console.log(d.id);
            return (
              <div key={d.id}>
                {/*----------------E ------- if you want to comment something in Return must do this*/}

                <div>
                  <b>{d.title}</b>
                </div>
                <div>{d.body}</div>
                <hr />
              </div>
            );
          })}
        <input
          type="text"
          ref={(VarName) => {
            this.txt1 = VarName;
          }}
        />
        <input
          type="text"
          ref={(VarName) => {
            this.txt2 = VarName;
          }}
        />
      </div>
    );
  }
}

export default App;
