import React, { Component } from "react";

class Header extends Component {
  componentWillMount() {
    console.log("Header: willMount");
  }

  componentDidMount() {
    console.log("Header: didMount");
  }

  render() {
    console.log("Header: render");
    return (
      <div>
        <h1>{this.props.children}</h1>
      </div>
    );
  }
}

export default Header;
