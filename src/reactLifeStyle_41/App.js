import React, { Component } from "react";
import Header from "./Header.js";
import FbButton from "./FbButton.js";

class App extends Component {
  state = { like: 0 };

  componentWillMount() {
    console.log("App:willMount");
  }

  componentDidMount() {
    console.log("App:didMount");
  }

  render() {
    console.log("App:render");
    return (
      <div>
        <Header title="I am Header" like={this.state.like} />
        <h4>Like : {this.state.like}</h4>
        <FbButton handleClick={this.onLike} caption="คลิกบวก Like" />
      </div>
    );
  }

  onLike = () => {
    this.setState((prevState) => {
      return {
        like: prevState.like + 1,
      };
    });
  };
}

export default App;
